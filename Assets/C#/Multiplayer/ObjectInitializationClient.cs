﻿using LiteNetLib;
using Multiplayer.Packets;
using UnityEngine;

namespace Multiplayer
{
    public partial class Client
    {
        public void OnObjectInitialization(ObjectInitializationPacket packet, NetPeer peer)
        {
            PhysicsManager.balls[packet.ID].transform.position = new UnityEngine.Vector3(packet.Position.X, packet.Position.Y, packet.Position.Z);
        }
        
        public void SynchronizeObjectPosition(int id, Vector3 position)
        {
            ObjectInitializationPacket objectInitializationPacket = new ObjectInitializationPacket(id,
                new System.Numerics.Vector3(position.x, position.y, position.z));
            
            server.Send(_netPacketProcessor.Write(objectInitializationPacket), DeliveryMethod.ReliableOrdered);
        }
    }
}
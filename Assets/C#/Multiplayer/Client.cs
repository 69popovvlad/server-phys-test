﻿using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using LiteNetLib;
using LiteNetLib.Utils;
using Multiplayer.Packets;
using UnityEngine;

namespace Multiplayer
{
    public partial class Client : INetEventListener
    {
        // Connect options
        public string serverIP = "127.0.0.1";
        public int serverPort = 5000;
        public string serverKey = "kbBYMBNTHIIe";

        public PhysicsManager PhysicsManager;

        public NetManager _netClient;
        private NetDataWriter _dataWriter;
        private NetPeer server;

        // Data recive
        private delegate void Packet_(NetPacketReader reader);

        private static Dictionary<long, Packet_> packets;
        private readonly NetPacketProcessor _netPacketProcessor = new NetPacketProcessor();

        // public Stage curStage = Stage.Wait;

        public Client(PhysicsManager physicsManager)
        {
            PhysicsManager = physicsManager;
            packets = new Dictionary<long, Packet_>();
            // packets.Add((long) PacketsName.GetIp, null);
            // packets.Add((long) PacketsName.CreateRoom, CreateRoom);
        }

        public void Start()
        {
            _dataWriter = new NetDataWriter();
            _netClient = new NetManager(this);
            _netClient.UnconnectedMessagesEnabled = true;
            _netClient.UpdateTime = 15;
            _netClient.Start();
            _netClient.Connect(serverIP, serverPort, serverKey);

            _netPacketProcessor.RegisterNestedType<SerializeVector>(() => new SerializeVector());
            
            // Subscribe to packet receiving
            _netPacketProcessor.SubscribeReusable<BattleStatusPacket, NetPeer>(OnGameStatusChanged);
            _netPacketProcessor.SubscribeReusable<ObjectInitializationPacket, NetPeer>(OnObjectInitialization);
        }
        
        public void OnUpdate()
        {
            _netClient.PollEvents();
            
            // if (Input.GetKeyDown(KeyCode.Q))
            // {
            //     _dataWriter.Reset();
            //     _dataWriter.Put(myNetworkID + "?" + roomKey);
            //     server.Disconnect(_dataWriter);
            // }

            server = _netClient.FirstPeer;
            if (server != null && server.ConnectionState == ConnectionState.Connected)
            {
                // server.Send(_netPacketProcessor.Write(variables), DeliveryMethod.ReliableOrdered);
            }
            else
            {
                Debug.Log("Sending discovery packet to endpoint");
                _netClient.SendBroadcast(new byte[] {1}, serverPort);
            }
        }

        void OnDestroy()
        {
            _netClient?.Stop();
        }

        public void OnPeerConnected(NetPeer peer)
        {
            server = peer;
            Debug.Log("[CLIENT] We connected to " + peer.EndPoint);
        }

        public void OnNetworkError(IPEndPoint endPoint, SocketError socketErrorCode)
        {
            Debug.Log("[CLIENT] We received error " + socketErrorCode);
        }

        public void OnNetworkReceive(NetPeer peer, NetPacketReader reader, DeliveryMethod deliveryMethod)
        {
            Packet_ packet;

            // Invoke function by identifier
            if (packets.TryGetValue(reader.CheckLong(), out packet))
            {
                reader.GetLong(); // Delete Long
                packet.Invoke(reader);
            }
            else
                _netPacketProcessor.ReadAllPackets(reader, peer);
        }

        public void OnNetworkReceiveUnconnected(IPEndPoint remoteEndPoint, NetPacketReader reader,
            UnconnectedMessageType messageType)
        {
            if (messageType == UnconnectedMessageType.BasicMessage && _netClient.ConnectedPeersCount == 0 && reader.GetInt() == 1)
            {
                Debug.Log("[CLIENT] Received discovery response. Connecting to: " + remoteEndPoint);
                _netClient.Connect(remoteEndPoint, serverKey);
            }
        }

        public void OnNetworkLatencyUpdate(NetPeer peer, int latency)
        {
        }

        public void OnConnectionRequest(ConnectionRequest request)
        {
        }

        public void OnPeerDisconnected(NetPeer peer, DisconnectInfo disconnectInfo)
        {
            Debug.Log("[CLIENT] We disconnected because " + disconnectInfo.Reason);
        }


        #region Rooms Functions
        // Client asked us to create a new Room
        public void CreateRoom(NetPacketReader packet)
        {
            // int networkID = packet.GetInt();
            // string nickName = packet.GetString();
            // string roomKey = packet.GetString();

            // // TODO: Проверку на наличие уже созданных комнат с таким RoomKey или с участием этого перца
            // allRooms.Add(new Room(roomKey, networkID, GetPeer(networkID)));
        }
        #endregion


        #region Getting packets
        // private void GetRacketPacket(RacketPacket packet, NetPeer peer)
        // {
        //     peer.Send(_netPacketProcessor.Write(packet), DeliveryMethod.ReliableOrdered);
        // }
        #endregion
    }
}
﻿using LiteNetLib;
using Multiplayer.Packets;

namespace Multiplayer
{
    public partial class Client
    {
        public void ChangeGameStatus(BattleStatus status)
        {
            BattleStatusPacket battleStatusPacket = new BattleStatusPacket()
            {
                BattleStatus = status
            };
            
            server.Send(_netPacketProcessor.Write(battleStatusPacket), DeliveryMethod.ReliableOrdered);
        }

        public void OnGameStatusChanged(BattleStatusPacket packet, NetPeer peer)
        {
            switch (packet.BattleStatus)
            {
                case BattleStatus.None:
                    break;
                
                case BattleStatus.Play:
                    break;
                
                case BattleStatus.Pause:
                    break;
                
                case BattleStatus.Reset:
                    break;
            }
        }
    }
}
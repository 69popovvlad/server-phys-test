﻿using LiteNetLib;
using Multiplayer.Packets;
using UnityEngine;

namespace Multiplayer
{
    public partial class Client
    {
        public void SynchronizeVectorInitialization(int id, Vector3 vector)
        {
            VectorInitializationPacket vectorInitializationPacket = new VectorInitializationPacket(id,
                new System.Numerics.Vector3(vector.x, vector.y, vector.z));
            
            server.Send(_netPacketProcessor.Write(vectorInitializationPacket), DeliveryMethod.ReliableOrdered);
        }
    }
}
﻿using System.Numerics;
using LiteNetLib.Utils;

namespace Multiplayer.Packets
{
    public class SerializeVector : INetSerializable
    {
        public float X { get; set; }
        public float Y { get; set; }
        public float Z { get; set; }

        public SerializeVector()
        {
        }

        public SerializeVector(Vector3 position)
        {
            X = position.X;
            Y = position.Y;
            Z = position.Z;
        }

        public void Serialize(NetDataWriter writer)
        {
            writer.Put(X);
            writer.Put(Y);
            writer.Put(Z);
        }

        public void Deserialize(NetDataReader reader)
        {
            X = reader.GetFloat();
            Y = reader.GetFloat();
            Z = reader.GetFloat();
        }

        public Vector3 GetVector()
        {
            return new Vector3(X, Y, Z);
        }
    }
}
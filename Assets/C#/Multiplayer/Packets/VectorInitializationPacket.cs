﻿using System.Numerics;

namespace Multiplayer.Packets
{
    public class VectorInitializationPacket
    {
        public int ID { get; set; }
        public SerializeVector SerializeVector { get; set; }

        public VectorInitializationPacket()
        {
        }

        public VectorInitializationPacket(int id, Vector3 vector)
        {
            ID = id;
            SerializeVector = new SerializeVector(vector);
        }
        
        public VectorInitializationPacket(Vector3 vector)
        {
            ID = -1;
            SerializeVector = new SerializeVector(vector);
        }
    }
}
﻿namespace Multiplayer.Packets
{
    public class BattleStatusPacket
    {
        public BattleStatus BattleStatus { get; set; }

        public BattleStatusPacket()
        {
        }

        public BattleStatusPacket(BattleStatus status)
        {
            BattleStatus = status;
        }
    }
    
    public enum BattleStatus
    {
            None,
            Play,
            Pause,
            Reset
    }
}
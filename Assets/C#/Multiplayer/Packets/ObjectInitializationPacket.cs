﻿using System.Numerics;

namespace Multiplayer.Packets
{
    public class ObjectInitializationPacket
    {
        public int ID { get; set; }
        public SerializeVector Position { get; set; }

        public ObjectInitializationPacket()
        {
        }

        public ObjectInitializationPacket(int id, Vector3 position)
        {
            ID = id;
            Position = new SerializeVector(position);
        }
        
        public ObjectInitializationPacket(Vector3 position)
        {
            ID = -1;
            Position = new SerializeVector(position);
        }
    }
}
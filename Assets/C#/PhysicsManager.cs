﻿using System.Collections.Generic;
using Multiplayer;
using Multiplayer.Packets;
using UnityEngine;

public class PhysicsManager : MonoBehaviour
{
    [SerializeField] private GameObject ballPrefab;
    private Vector3 lastBallPosition;
    public List<GameObject> balls = new List<GameObject>();

    private Client client;

    void Start()
    {
        client = new Client(this);
        client.Start();
    }

    void Update()
    {
        client.OnUpdate();
        
        if (Input.GetMouseButtonDown(0))
            SpawnBall();

        if (Input.GetMouseButtonDown(1))
            SetVector();

        if (Input.GetKeyDown(KeyCode.D))
            SceneClear();

        if (Input.GetKeyDown(KeyCode.S))
            StartSimulation();

        if (Input.GetKeyDown(KeyCode.R))
            StopSimulation();
    }

    private void SpawnBall()
    {
        lastBallPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        lastBallPosition.z = 0;

        balls.Add(Instantiate(ballPrefab, lastBallPosition, Quaternion.identity));
        client.SynchronizeObjectPosition(balls.Count - 1, lastBallPosition);
    }

    private void SetVector()
    {
        Vector3 vector = (Camera.main.ScreenToWorldPoint(Input.mousePosition) - lastBallPosition).normalized;

        client.SynchronizeVectorInitialization(balls.Count - 1, vector);
    }

    private void StartSimulation()
    {
        client.ChangeGameStatus(BattleStatus.Play);
    }

    private void StopSimulation()
    {
        client.ChangeGameStatus(BattleStatus.Pause);
    }

    private void SceneClear()
    {
        client.ChangeGameStatus(BattleStatus.Reset);
        
        foreach (var ball in balls)
            Destroy(ball);

        balls.Clear();
    }
}